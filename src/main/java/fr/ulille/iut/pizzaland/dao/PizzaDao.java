package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL,base VARCHAR NOT NULL)")
	void createTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation(PizzaID VARCHAR(128), ingredientID VARCHAR(128),"
			+ "FOREIGN KEY (pizzaID) references pizzas(id)," + 
			"  FOREIGN KEY (ingredientID) references ingredients(id)," + 
			"  PRIMARY KEY (pizzaID, ingredientID))")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createTable();
	}


	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropTable();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
	public void dropPizzaIngredientAssociationTable();
	
	@SqlUpdate("INSERT INTO pizzas (id, name,base) VALUES (:id, :name, :base)")
	void insert(@BindBean Pizza pizza);

	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void remove(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM pizzas WHERE base = :base")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByIngredients(@Bind("ingredients") Ingredient ingredient);
}
