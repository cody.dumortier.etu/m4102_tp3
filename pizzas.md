/pizzas 					GET		liste des pizzas.

/pizzas/{id}  				GET  	une pizza.

/pizzas/{id}/ingredients  	GET		liste des ingrédients de la pizza.

/pizzas   					POST	création d'une nouvelle pizza.

/pizzas/{id}   				DELETE	supprime la pizza de la liste.



{
"id" : "f38806a8-7c85-49ef-980c-149dcd81d306",
"name" : "Calzone",
"ingrédients" : "Mozarella","jambon","champignon"
}

